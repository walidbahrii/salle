<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/SalleReunion');
});
Route::resource('SalleReunion', 'UtilisateurController');
Route::get('/', 'UtilisateurController@index');
Route::get('/ListeEntreprise', 'UtilisateurController@listeEntr');
Route::get('/ListeSalle', 'UtilisateurController@liste');